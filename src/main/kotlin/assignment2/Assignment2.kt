package assignment2

import java.awt.BorderLayout
import java.awt.FlowLayout
import java.awt.GridLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.WindowConstants

class Assignment2 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println("Hello, World!")

            val frame = JFrame()
            val n = JButton("North")
            val e = JButton("East")
            val w = JButton("West")
            val c = JButton("Center")
            frame.layout = BorderLayout()
            frame.add(n, BorderLayout.NORTH)
            frame.add(e, BorderLayout.EAST)
            frame.add(w, BorderLayout.WEST)
            frame.add(c, BorderLayout.CENTER)

            val panel = JPanel()
            panel.layout = FlowLayout(FlowLayout.RIGHT)
            val panel2 = JPanel()
            panel2.layout = GridLayout(1,0)
            val ok = JButton("Ok")
            val cancel = JButton("Cancel")
            panel2.add(ok)
            panel2.add(cancel)
            panel.add(panel2)
            frame.add(panel, BorderLayout.SOUTH)
            frame.pack()
            frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
            frame.isVisible = true

            // The following should create and display the same GUI as above
            // Use Kotlin's support for DSLs to define functions and builder
            //    classes for the following DSL
            // Be sure to use @DslMarker to limit scoping!
            // The only top-level function you should implement is frame();
            //   all other functions must be in a builder class
            // Note that you will need to implement a enum class for
            //   FlowAlignment (with values Left, Center and Right)
            // You can add other enum classes if you would like
            // Note that we no longer need end() or show() calls, as our
            //   new DSL provides explicit scoping
            // (I wanted to have you implement the directions like
            //       Direction.North contains button("NORTH")
            //   but that would have required button() being in scope
            //   in the border layout, which I want to avoid, which gives
            //   us more error checking at compile-time
            //   Ahhh... tradeoffs!)

//            frame {
//                north {
//                    button("NORTH")
//                }
//                east {
//                    button("EAST")
//                }
//                west {
//                    button("WEST")
//                }
//                center {
//                    button("CENTER")
//                }
//                south {
//                    grid(1, 0) {
//                        flow(FlowAlignment.Right) {
//                            button("Ok")
//                            button("Cancel")
//                        }
//                    }
//                }
//            }
        }
    }
}