package dsl212

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val machine = xx@ stateMachine {
                startState = States.NotPowered
                state {
                    stateId = States.NotPowered
                    transition {
//                        startState = States.Powered   // UH OH!!!!!!!!!
                        eventId = Events.ElectricOn
                        nextStateId = States.Powered
                    }
                }
                state {
                    stateId = States.Powered
                    transition {
                        eventId = Events.ElectricOff
                        nextStateId = States.NotPowered
                    }
                    transition {
                        eventId = Events.Clap
                        nextStateId = States.LightsOn
                        action = { println("Clap!!!!!") }
                    }
                }
                state {
                    stateId = States.LightsOn
                    transition {
                        eventId = Events.ElectricOff
                        nextStateId = States.NotPowered
                    }
                    transition {
                        eventId = Events.Clap
                        nextStateId = States.Powered
                    }
                }
            }

            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

fun stateMachine(init : StateMachineBuilder.() -> Unit) : StateMachine {
    val builder = StateMachineBuilder()
    builder.init()
    return builder.build()
}

@DslMarker
annotation class BuilderThing1
@DslMarker
annotation class BuilderThing2

@BuilderThing2
class StateBuilder {
    private val actions = mutableMapOf<IEvent, ()->Unit>()
    private val transitions = mutableMapOf<IEvent, IState>()
    var stateId : IState? = null

    fun transition(init : TransitionBuilder.() -> Unit) {
        val builder = TransitionBuilder()
        builder.init()
        val transitionStuff = builder.build()
        transitions[transitionStuff.eventId] = transitionStuff.nextStateId
        transitionStuff.action?.let {
            actions[transitionStuff.eventId] = it
        }
    }
    fun build() =
        stateId?.let {
            val state = State(it)
            state.transitions.putAll(transitions)
            state
        } ?: throw IllegalStateException("stateId was not assigned")
}

@BuilderThing1
class TransitionBuilder {
    var eventId : IEvent? = null
    var nextStateId : IState? = null
    var action : (() -> Unit)? = null
    fun build() =
        eventId?.let { event ->
            nextStateId?.let { nextState ->
                TransitionStuff(event, nextState, action) // action is optional
            } ?: throw IllegalStateException("nextStateId not specified for transition")
        } ?: throw IllegalStateException("eventId not specified for transition")
}

class TransitionStuff(
    var eventId: IEvent,
    var nextStateId: IState,
    var action: (() -> Unit)?
)

@BuilderThing1
class StateMachineBuilder {
    private val machine = StateMachine()
    var startState : IState? = null

    fun state(init : StateBuilder.()->Unit) {
        val builder = StateBuilder()
        builder.init()
        val state = builder.build()
        machine.states[state.stateId] = state
    }
    fun build() = machine.apply { currentState = startState }
}

class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                state.actions[eventId]?.invoke()
                currentState = nextState
            }
            println("State changed to $currentState")
        }
    }
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
    val actions = mutableMapOf<IEvent, () -> Unit>()
}