package dsl212

abstract class Department(val name : String) {
    open fun report(indent : String) {
        println("$indent ${getReport()}")
    }
    abstract fun getReport() : String
}

abstract class CompositeDepartment(name : String) : Department(name) {
    val children = mutableListOf<Department>()
    final override fun report(indent : String) {
        super.report(indent)
        children.forEach { it.report("$indent  ") }
    }
}

class APL : CompositeDepartment("APL") {
    override fun getReport()= "APL is a fantastic place to work!"
}
class AOS : CompositeDepartment("AOS") {
    override fun getReport()= "AOS Rocks!"
}
class QAI : Department("QAI") {
    override fun getReport()= "QAI Rules!"
}
class SES : CompositeDepartment("SES") {
    override fun getReport()= "SES is way out there!"
}
class ABC : Department("ABC") {
    override fun getReport()= "Yawn"
}
class DEF : Department("DEF") {
    override fun getReport()= "Hiccup"
}

fun main(args: Array<String>) {
    val apl = APL().apply {
        children.add(AOS().apply {
            children.add(QAI())
        })
        children.add(SES().apply {
            children.add(ABC())
            children.add(DEF())
        })
    }
    apl.report("")
}
