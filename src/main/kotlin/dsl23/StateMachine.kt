package dsl23

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val machine = object : StateMachineBuilder() {
                override fun build(): StateMachine {
                    return stateMachine(
                        startState(States.NotPowered),
                        state(States.LightsOn,
                            on(Events.ElectricOff,
                                goto(States.NotPowered)),
                            on(Events.Clap,
                                goto(States.Powered))),
                        state(States.Powered,
                            on(Events.ElectricOff,
                                goto(States.NotPowered)),
                            on(Events.Clap,
                                goto(States.LightsOn))),
                        state(States.NotPowered,
                            on(Events.ElectricOn,
                                goto(States.Powered))))
                }
            }.build()


            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

abstract class StateMachineBuilder {
    abstract fun build() : StateMachine

    protected fun stateMachine(startStateId : IState,
                               vararg states : State) : StateMachine {
        val machine = StateMachine()
        machine.currentState = startStateId
        states.forEach {
            machine.states[it.stateId] = it
        }
        return machine
    }
    protected fun state(stateId : IState,
                        vararg transitions : Pair<IEvent, IState>) : State {
        val state = State(stateId)
        state.transitions.putAll(transitions)
        return state
    }
    protected fun on(eventId : IEvent, stateId : IState) = Pair(eventId, stateId)

    protected fun goto(stateId : IState) = stateId

    protected fun startState(stateId : IState) = stateId
}



class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                currentState = nextState

            }
            println("State changed to $currentState")
        }
    }
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
}
