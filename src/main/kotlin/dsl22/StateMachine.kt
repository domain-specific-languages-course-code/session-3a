package dsl22

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {


        @JvmStatic
        fun main(args: Array<String>) {
            val machine = StateMachineBuilder().stateMachine()
                    .state(States.LightsOn)
                        .on(Events.ElectricOff)
                            .goto(States.NotPowered)
                        .on(Events.Clap)
                            .goto(States.Powered)
                    .state(States.Powered)
                        .on(Events.ElectricOff)
                            .goto(States.NotPowered)
                        .on(Events.Clap)
                            .goto(States.LightsOn)
                    .state(States.NotPowered)
                        .on(Events.ElectricOn)
                            .goto(States.Powered)
                    .startState(States.NotPowered)

            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

class StateMachineBuilder {
    private var machine : StateMachine? = null
    private var currentState : State? = null
    private var currentEventId : IEvent? = null

    fun stateMachine() : StateMachineBuilder {
        machine = StateMachine()
        return this
    }
    fun state(stateId : IState) : StateMachineBuilder {
        currentState = State(stateId)
        machine!!.states[stateId] = currentState!!
        return this
    }
    fun on(eventId : IEvent) : StateMachineBuilder {
        currentEventId = eventId
        return this
    }
    fun goto(stateId : IState) : StateMachineBuilder {
        currentEventId?.let { event ->
            currentState?.let { state ->
                state.transitions[event] = stateId
            } ?: throw IllegalStateException("You didn't define the current state before the on/goto")
        } ?: throw IllegalStateException("You didn't define the 'on' before the 'goto'")
        return this
    }
    fun startState(stateId : IState) : StateMachine {
        machine?.let {
            it.currentState = stateId
        } ?: throw IllegalStateException("You called startState() before stateMachine()")
        return machine!!
    }
}



class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                currentState = nextState

            }
            println("State changed to $currentState")
        }
    }
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
}
