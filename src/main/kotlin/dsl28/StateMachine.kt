package dsl28

interface IEvent
interface IState

sealed class A {
    class B : A() {}
    class C : A() {}
    class D : A() {}
}
class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val machine = stateMachine {
                startState = States.NotPowered
                States.NotPowered.create {
                    Events.ElectricOn goesTo States.Powered
                }
                States.Powered.create {
                    Events.ElectricOff goesTo States.NotPowered
                    Events.Clap goesTo States.LightsOn andDoes { println("Clap!!!")}
                }
                States.LightsOn.create {
                    Events.ElectricOff goesTo States.NotPowered
                    Events.Clap goesTo States.Powered
                }
            }

            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

fun stateMachine(init : StateMachineBuilder.() -> Unit) : StateMachine {
    val builder = StateMachineBuilder()
    builder.init()
    return builder.build()
}

class StateBuilder(stateId : IState) {
    val state = State(stateId)
    infix fun IEvent.goesTo(stateId : IState) : TransitionThing {
        state.transitions[this] = stateId
        return TransitionThing(state, this)
    }
    fun build() = state
}

infix fun TransitionThing.andDoes(action : () -> Unit) {
    state.actions[eventId] = action
}
class TransitionThing(val state : State, val eventId : IEvent)

class StateMachineBuilder {
    val machine = StateMachine()
    var startState : IState? = null

    fun IState.create(init : StateBuilder.()->Unit) {
        val builder = StateBuilder(this)
        builder.init()
        val state = builder.build()
        machine.states[state.stateId] = state
    }
    fun build() = machine.apply { currentState = startState }
}


class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                state.actions[eventId]?.invoke()
                currentState = nextState
            }
            println("State changed to $currentState")
        }
    }
}



class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
    val actions = mutableMapOf<IEvent, () -> Unit>()
}