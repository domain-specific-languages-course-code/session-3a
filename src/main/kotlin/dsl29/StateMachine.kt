package dsl29

//sealed class Stuff
//class Foo : Stuff() {
//    var name : String? = null
//}
//class Fee : Stuff() {
//    var age : Int = 0
//}

sealed class MyEvent : Event()
object ElectricOn : MyEvent()
object ElectricOff : MyEvent()
object Clap : MyEvent()

sealed class MyState : State()
object Powered : MyState()
object LightsOn : MyState()
object NotPowered : MyState()

class States {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
//            val f : Stuff = Foo()
//            val x = when (f) {
//                is Foo -> f.name?.length ?: 0
//                is Fee -> f.age
//            }

            val machine = stateMachine {
                currentState = NotPowered
                NotPowered transitions {
                    ElectricOn goesTo Powered
                }
                Powered transitions {
                    ElectricOff goesTo NotPowered
                    Clap goesTo LightsOn andDoes { println("Clap!!!")}
                }
                LightsOn transitions {
                    ElectricOff goesTo NotPowered
                    Clap goesTo Powered
                }
            }

            machine handle ElectricOn
            machine handle ElectricOff
            machine handle ElectricOn
            machine handle Clap
            machine handle Clap
        }
    }
}

fun stateMachine(init : StateMachine.() -> Unit) : StateMachine {
    val machine = StateMachine()
    machine.init()
    return machine
}

class Transition(val event : Event, val state : State)

open class Event {
    override fun toString(): String {
        return javaClass.simpleName
    }
}
open class State {
    val transitions = mutableMapOf<Event, Transition>()
    val actions = mutableMapOf<Event, () -> Unit>()
    infix fun Event.goesTo(nextState : State) : Transition {
        val transition = Transition(this, nextState)
        this@State.transitions[this] = transition
        return transition
    }
    override fun toString(): String {
        return javaClass.simpleName
    }
}
class StateMachine {
    var currentState : State? = null
}

// infix functions to make the language look more language-y
// we made these extension functions just to get the syntax highlighting...
//   these could just as easily have been defined inside the objects themselves
//   (and probably should be...)
infix fun State.transitions(init : State.()->Unit) {
    init()
}
infix fun StateMachine.handle(event : Event) {
    currentState?.let { state ->
        state.transitions[event]?.let { transition ->
            state.actions[event]?.invoke()
            currentState = transition.state
        }
        println("State changed to $currentState")
    }
}
infix fun Transition.andDoes(action : () -> Unit) {
    state.actions[event] = action
}
