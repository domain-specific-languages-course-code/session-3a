package dsl27

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val machine = StateMachine().apply {
                currentState = States.NotPowered
                add(State(States.NotPowered).apply {
                    Events.ElectricOn goesTo States.Powered
                })
                add(State(States.Powered).apply {
                    Events.ElectricOff goesTo States.NotPowered
                    Events.Clap goesTo States.LightsOn andDoes { println("Clap!!!")}
                })
                add(State(States.LightsOn).apply {
                    Events.ElectricOff goesTo States.NotPowered
                    Events.Clap goesTo States.Powered
                })
            }

            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

class StateMachineBuilder : A, B, C, D, E {
    override fun action(action: () -> Unit): E {
        currentState!!.actions[currentEventId!!] = action
        return this
    }

    private var machine : StateMachine? = null
    private var currentState : State? = null
    private var currentEventId : IEvent? = null

    override fun stateMachine() : B {
        machine = StateMachine()
        return this
    }
    override fun state(stateId : IState) : C {
        currentState = State(stateId)
        machine!!.states[stateId] = currentState!!
        return this
    }
    override fun on(eventId : IEvent) : D {
        currentEventId = eventId
        return this
    }
    override fun goto(stateId : IState) : C {
        currentEventId?.let { event ->
            currentState?.let { state ->
                state.transitions[event] = stateId
            } ?: throw IllegalStateException("You didn't define the current state before the on/goto")
        } ?: throw IllegalStateException("You didn't define the 'on' before the 'goto'")
        return this
    }
    override fun startState(stateId : IState) : StateMachine {
        machine?.let {
            it.currentState = stateId
        } ?: throw IllegalStateException("You called startState() before stateMachine()")
        return machine!!
    }
}



class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun add(state : State) {
        states[state.stateId] = state
    }
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                state.actions[eventId]?.invoke()
                currentState = nextState
            }
            println("State changed to $currentState")
        }
    }
}

infix fun TransitionThing.andDoes(action : () -> Unit) {
    state.actions[eventId] = action
}
class TransitionThing(val state : State, val eventId : IEvent) {
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
    val actions = mutableMapOf<IEvent, () -> Unit>()
    infix fun IEvent.goesTo(stateId : IState) : TransitionThing {
        transitions[this] = stateId
        return TransitionThing(this@State, this)
    }
}


interface A {
    fun stateMachine() : B
}
interface B {
    fun state(stateId : IState) : C
    fun startState(stateId : IState) : StateMachine
}
interface C {
    fun state(stateId : IState) : C
    fun on(eventId : IEvent) : D
    fun startState(stateId : IState) : StateMachine
}
interface D {
    fun goto(stateId : IState) : C
    fun action(action : () -> Unit) : E
}
interface E {
    fun goto(stateId : IState) : C
}