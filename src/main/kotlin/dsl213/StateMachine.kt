package dsl213

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val machine = stateMachine {
                startStateId = States.NotPowered
                state {
                    stateId = States.NotPowered
                    transition {
                        eventId = Events.ElectricOn
                        nextStateId = States.Powered
                    }
                }
                state {
                    stateId = States.Powered
                    transition {
                        eventId = Events.ElectricOff
                        nextStateId = States.NotPowered
                    }
                    transition {
                        eventId = Events.Clap
                        nextStateId = States.LightsOn
                        action = { println("Clap!!!!!") }
                    }
                }
                state {
                    stateId = States.LightsOn
                    transition {
                        eventId = Events.ElectricOff
                        nextStateId = States.NotPowered
                    }
                    transition {
                        eventId = Events.Clap
                        nextStateId = States.Powered
                    }
                }
            }

            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

fun stateMachine(init : StateMachineBuilder.() -> Unit) =
    StateMachineBuilder().let {
        it.init()
        it.build()
    }


//NOTE - we only need the DslMarker on the top-level class (if there is a common superclass)
@DslMarker
annotation class BuilderMarker

@BuilderMarker
abstract class Builder {
    abstract fun build() : Any
}

abstract class CompositeBuilder : Builder() {
    val children = mutableListOf<Builder>()
    fun <T:Builder> doInit(builder : T, init : T.() -> Unit) : T =
            builder.also {
                it.init()
                children.add(it)
            }
}

class StateBuilder : CompositeBuilder() {
    var stateId : IState? = null

    fun transition(init : TransitionBuilder.() -> Unit) = doInit(TransitionBuilder(), init)

    override fun build() =
        stateId?.let {
            val state = State(it)
            val childResults = children.map { it.build() }
            val transitions = childResults.filterIsInstance<TransitionStuff>() // not really needed; only one child type
            transitions.forEach { transition ->
                state.transitions[transition.eventId] = transition.nextStateId
                transition.action?.let {action ->
                    state.actions[transition.eventId] = action
                }
            }
            state
        } ?: throw IllegalStateException("stateId was not assigned")
}

class TransitionBuilder : Builder() {
    var eventId : IEvent? = null
    var nextStateId : IState? = null
    var action : (() -> Unit)? = null

    override fun build() =
        eventId?.let { event ->
            nextStateId?.let { nextState ->
                TransitionStuff(event, nextState, action) // action is optional
            } ?: throw IllegalStateException("nextStateId not specified for transition")
        } ?: throw IllegalStateException("eventId not specified for transition")
}

class TransitionStuff(
    var eventId: IEvent,
    var nextStateId: IState,
    var action: (() -> Unit)?
)

class StateMachineBuilder : CompositeBuilder() {
    var startStateId : IState? = null

    fun state(init : StateBuilder.()->Unit) = doInit(StateBuilder(), init)

    override fun build() =
        startStateId?.let { startState ->
            StateMachine().apply {
                currentState = startState
                children.forEach { childBuilder ->
                    // different approach - if we know we have exactly one child type
                    //   we can just walk each rather than filter as above
                    val state = childBuilder.build() as State
                    states[state.stateId] = state
                }
            }
        } ?: throw IllegalStateException("startStateId not specified for stateMachine")
}



class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                state.actions[eventId]?.invoke()
                currentState = nextState
            }
            println("State changed to $currentState")
        }
    }
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
    val actions = mutableMapOf<IEvent, () -> Unit>()
}