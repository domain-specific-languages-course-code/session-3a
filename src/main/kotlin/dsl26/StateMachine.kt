package dsl26

interface IEvent
interface IState

class States {
    enum class Events : IEvent { ElectricOn, ElectricOff, Clap}
    enum class States : IState { NotPowered, Powered, LightsOn}

    companion object {


        @JvmStatic
        fun main(args: Array<String>) {
            val machine = StateMachineBuilder().stateMachine()
                    .state(States.LightsOn)
                        .on(Events.ElectricOff)
                            .goto(States.NotPowered)
                        .on(Events.Clap)
                            .goto(States.Powered)
                    .state(States.Powered)
                        .on(Events.ElectricOff)
                            .goto(States.NotPowered)
                        .on(Events.Clap)
                            .action { println("Clap") }
                            .goto(States.LightsOn)
                    .state(States.NotPowered)
                        .on(Events.ElectricOn)
                            .goto(States.Powered)
                    .startState(States.NotPowered)

            machine.handle(Events.ElectricOn)
            machine.handle(Events.ElectricOff)
            machine.handle(Events.ElectricOn)
            machine.handle(Events.Clap)
            machine.handle(Events.Clap)
        }


    }
}

class StateMachineBuilder : A, B, C, D, E {
    override fun action(action: () -> Unit): E {
        currentState!!.actions[currentEventId!!] = action
        return this
    }

    private var machine : StateMachine? = null
    private var currentState : State? = null
    private var currentEventId : IEvent? = null

    override fun stateMachine() : B {
        machine = StateMachine()
        return this
    }
    override fun state(stateId : IState) : C {
        currentState = State(stateId)
        machine!!.states[stateId] = currentState!!
        return this
    }
    override fun on(eventId : IEvent) : D {
        currentEventId = eventId
        return this
    }
    override fun goto(stateId : IState) : C {
        currentEventId?.let { event ->
            currentState?.let { state ->
                state.transitions[event] = stateId
            } ?: throw IllegalStateException("You didn't define the current state before the on/goto")
        } ?: throw IllegalStateException("You didn't define the 'on' before the 'goto'")
        return this
    }
    override fun startState(stateId : IState) : StateMachine {
        machine?.let {
            it.currentState = stateId
        } ?: throw IllegalStateException("You called startState() before stateMachine()")
        return machine!!
    }
}



class StateMachine {
    val states = mutableMapOf<IState, State>()
    var currentState : IState? = null
    fun handle(eventId : IEvent) {
        states[currentState]?.let { state ->
            state.transitions[eventId]?.let {nextState ->
                state.actions[eventId]?.invoke()
                currentState = nextState
            }
            println("State changed to $currentState")
        }
    }
}

class State(val stateId : IState) {
    val transitions = mutableMapOf<IEvent, IState>()
    val actions = mutableMapOf<IEvent, () -> Unit>()
}


interface A {
    fun stateMachine() : B
}
interface B {
    fun state(stateId : IState) : C
    fun startState(stateId : IState) : StateMachine
}
interface C {
    fun state(stateId : IState) : C
    fun on(eventId : IEvent) : D
    fun startState(stateId : IState) : StateMachine
}
interface D {
    fun goto(stateId : IState) : C
    fun action(action : () -> Unit) : E
}
interface E {
    fun goto(stateId : IState) : C
}