package dsl2

import java.awt.GridLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.WindowConstants

class Sample {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println("Hello, World!")
            JFrame().apply {
                layout = GridLayout(0,2)
                add(JButton("A"))
                add(JButton("B"))
                add(JButton("C"))
                add(JButton("D"))
                add(JButton("E"))
                add(JButton("F"))
                pack()
                defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
                isVisible = true
            }
        }
    }
}