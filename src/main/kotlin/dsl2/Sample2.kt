package dsl2

import java.awt.*
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.WindowConstants

class Sample2 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println("Hello, World!")

            frame {
                north {
                    button("NORTH")
                }
                east {
                    button("EAST")
                }
                west {
                    button("WEST")
                }
                center {
                    button("CENTER")
                }
                south {
                    grid(1, 0) {
                        flow(FlowAlignment.Right) {
                            button("Ok")
                            button("Cancel")
                        }
                    }
                }
            }
        }

        private fun frame(init: FrameBuilder.() -> Unit) =
            FrameBuilder().apply {
                init()
                build()
            }
    }
}

@DslMarker
annotation class BuilderMarker

@BuilderMarker
abstract class Builder {
    abstract fun build() : Any
}

abstract class CompositeBuilder<CONTAINER:Container> : Builder() {
    private val children = mutableListOf<Builder>()
    fun <T:Builder> doInit(builder : T, init : T.() -> Unit) =
            builder.also {
                builder.init()
                children.add(builder)
            }
    override fun build() =
            createContainer().apply {
                layout = createLayout()
                children.forEach {
                    addChild(this, it.build())
                }
                afterChildrenAdded()
            }
    protected abstract fun createContainer() : CONTAINER
    protected abstract fun createLayout() : LayoutManager
    protected open fun CONTAINER.afterChildrenAdded() {}
    protected abstract fun addChild(container : CONTAINER, value : Any)
}

abstract class BorderBuilderBase<CONTAINER:Container> : CompositeBuilder<CONTAINER>() {
    fun north(init : DirectionBuilder.() -> Unit) = doInit(DirectionBuilder(BorderLayout.NORTH), init)
    fun south(init : DirectionBuilder.() -> Unit) = doInit(DirectionBuilder(BorderLayout.SOUTH), init)
    fun east(init : DirectionBuilder.() -> Unit) = doInit(DirectionBuilder(BorderLayout.EAST), init)
    fun west(init : DirectionBuilder.() -> Unit) = doInit(DirectionBuilder(BorderLayout.WEST), init)
    fun center(init : DirectionBuilder.() -> Unit) = doInit(DirectionBuilder(BorderLayout.CENTER), init)
    override fun createLayout() = BorderLayout()
    override fun addChild(container: CONTAINER, value: Any) {
        @Suppress("UNCHECKED_CAST")
        val pair = value as Pair<String, Component>
        container.add(pair.second, pair.first)
    }
}

class FrameBuilder : BorderBuilderBase<JFrame>() {
    override fun createContainer() = JFrame()
    override fun JFrame.afterChildrenAdded() {
        pack()
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        isVisible = true
    }
}

class BorderBuilder : BorderBuilderBase<JPanel>() {
    override fun createContainer() = JPanel()
}

abstract class NonBorderBuilder : CompositeBuilder<JPanel>() {
    fun grid(rows : Int, cols : Int, init : GridBuilder.() -> Unit) = doInit(GridBuilder(rows, cols), init)
    fun flow(alignment : FlowAlignment, init : FlowBuilder.() -> Unit) = doInit(FlowBuilder(alignment), init)
    fun border(init : BorderBuilder.() -> Unit) = doInit(BorderBuilder(), init)
    fun button(text : String) = doInit(ButtonBuilder(text), {})
    override fun createContainer() = JPanel()
    override fun addChild(container: JPanel, value: Any) {
        container.add(value as Component)
    }
}

class GridBuilder(private val rows : Int,
                  private val cols : Int) : NonBorderBuilder() {
    override fun createLayout() = GridLayout(rows, cols)
}

enum class FlowAlignment(val value : Int) {
    Left(FlowLayout.LEFT),
    Center(FlowLayout.CENTER),
    Right(FlowLayout.RIGHT)
}

class FlowBuilder(private val alignment : FlowAlignment) : NonBorderBuilder() {
    override fun createLayout() = FlowLayout(alignment.value)
}

enum class Direction(val value : String) {
    North(BorderLayout.NORTH),
    South(BorderLayout.SOUTH),
    East(BorderLayout.EAST),
    West(BorderLayout.WEST),
    Center(BorderLayout.CENTER)
}

class DirectionBuilder(private val direction : String) : Builder() {
    private var componentBuilder : Builder? = null

    private fun <T:Builder> doInit(builder : T, init : T.() -> Unit) =
            builder.also {
                if (componentBuilder != null) {
                    throw IllegalStateException("More than one component set in $direction {...}")
                }
                componentBuilder = builder
                builder.init()
            }

    fun grid(rows : Int, cols : Int, init : GridBuilder.() -> Unit) = doInit(GridBuilder(rows, cols), init)
    fun flow(alignment : FlowAlignment, init : FlowBuilder.() -> Unit) = doInit(FlowBuilder(alignment), init)
    fun border(init : BorderBuilder.() -> Unit) = doInit(BorderBuilder(), init)
    fun button(text : String) = doInit(ButtonBuilder(text), {})

    override fun build() : Pair<String, Component> =
            componentBuilder?.let {
                Pair(direction, it.build() as Component)

            } ?: throw IllegalStateException("$direction must have exactly one child")
}



//class DirectionBuilder(private val value : String) : ContainerBuilder() {
//    private var componentBuilder : Builder? = null
//
//    override fun <T : Builder> afterInit(builder: T) {
//        if (componentBuilder != null) {
//            throw IllegalStateException("More than one component set in $value {...}")
//        }
//        componentBuilder = builder
//    }
//
//    override fun build() : Pair<String, Component> =
//        componentBuilder?.let {
//            Pair(value, it.build() as Component)
//
//        } ?: throw IllegalStateException("$value must have exactly one child")
//}

class ButtonBuilder(private val text : String) : Builder() {
    override fun build() = JButton(text)
}